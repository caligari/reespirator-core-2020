/** PressureControl.hpp
 *
 * Ventilation in PC (preassure control).
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _PRESSURE_CONTROL_HPP
#define _PRESSURE_CONTROL_HPP

#include <reespirator/Core/DRE.hpp>

namespace reespirator
{

class PressureControl
{
    public:

        void setCycle(u16 insTenths, u16 exTenths, const MachineParams* params);

        u16 updateInspiration(u16 currentTenths);
        u16 updateExpiration(u16 currentTenths);

    private: 

        u16 _peep;
        u16 _peak;
        u16 _peakTenths;
        u32 _m;

};

}; // namespace reespirator

#endif // _PRESSURE_CONTROL_HPP
