/** PressureControl.cpp
 *
 * Ventilation in PC (preassure control).
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "PressureControl.hpp"

namespace reespirator
{

void PressureControl::setCycle(u16 insTenths, u16 exTenths, const MachineParams* params)
{
    // y range
    _peep = params->getPeepPressure();
    _peak = params->getPeakPressure();

    // x range
    _peakTenths = ((u32)insTenths * params->getRamp()) / 255;

    // slope of the line (precission x16)
    _m = ((_peak - _peep) << 4) / _peakTenths;
}


u16 PressureControl::updateInspiration(u16 currentTenths)
{
    // after the ramp
    if (currentTenths >= _peakTenths) return _peak;

    // linear equation for ramp
    return ((_m * currentTenths) >> 4) + _peep;
}


u16 PressureControl::updateExpiration(u16 currentTenths)
{
    // expiration is not pressure driven
    return _peep;
}


}; // namespace reespirator