/** ReespiratorFsm.hpp
 *
 * Main FSM for Reespirator machine.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _REESPIRATOR_FSM_HPP
#define _REESPIRATOR_FSM_HPP

#include <LoopTicker.hpp>
#include <reespirator/Core/DRE.hpp>
#include <reespirator/SerialMonitor/Hmi01Fsm.hpp>
#include <reespirator/Mock/SensorsBuffer.hpp>
#include <reespirator/Fsm/VentilationFsm.hpp>
#include <reespirator/Fsm/SettingsManagerFsm.hpp>

namespace reespirator
{

class ReespiratorFsm
{
    public:

        ReespiratorFsm(DRE& dre);

        /**
         * @brief Task entry-point to process incoming frames and send outgoing frames.
         *
         * @param now_millis
         */
        void update(u64 now_millis);

        /**
         * @brief Static class method to allow the deference of the object instance.
         *
         * @param object_ptr Pointer to the ReespiratorFsm instance.
         * @param loop_ticker Pointer to the LoopTicker helper calling this method.
         */
        static void tickUpdate(const void* object_ptr, LoopTicker* loop_ticker)
        {
            ((ReespiratorFsm*) object_ptr)->update(loop_ticker->getLoopMs64());
        }

        /**
         * @brief Non-blocking execution of FSM.
         *
         * @return true FSM state was changed.
         * @return false FSM state was not changed.
         */
        bool run();

        enum State
        {
            State_None,
            State_Initializing,
            State_Running,
            State_Halted,
        };

        State getState() { return _state; }

        SettingsManagerFsm& getSettings() { return _settings; }
        SensorsBuffer& getSensors() { return _sensors; }
        VentilationFsm& getVentilation() { return _ventilation; }
        Hmi01Fsm& getSerialMonitor() { return _serialMonitor; }

    private:

        DRE& _dre;

        // subsystems
        SettingsManagerFsm _settings;
        SensorsBuffer _sensors;
        VentilationFsm _ventilation;
        Hmi01Fsm _serialMonitor;

        State _state;
        RuntimeSystem _runtime;

        bool _initSubsystems();
};

}; // namespace reespirator

#endif // _REESPIRATOR_FSM_HPP