/** ReespiratorFsm.cpp
 *
 * Main FSM for ventilation machine.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "ReespiratorFsm.hpp"

// don't touch: this define is passed by environment
#ifndef FIRMWARE_VERSION_MAJOR
#define FIRMWARE_VERSION_MAJOR 0
#endif

// don't touch: this define is passed by environment
#ifndef FIRMWARE_VERSION_MINOR
#define FIRMWARE_VERSION_MINOR 0
#endif

namespace reespirator
{

ReespiratorFsm::ReespiratorFsm(DRE& dre) :
    _dre(dre),
    _settings(dre),
    _sensors(dre),
    _ventilation(dre),
    _serialMonitor(dre)
{
    _state = State_None;

    dre.runtime = &_runtime;
    _runtime.setFirmwareVersion((FIRMWARE_VERSION_MAJOR << 8) | FIRMWARE_VERSION_MINOR);
}


void ReespiratorFsm::update(u64 now_millis)
{
    _runtime.setLoopMillis64(now_millis);

    run();
}


bool ReespiratorFsm::run()
{
    State previous = _state;

    switch (_state)
    {
        case State_None:
            _state = State_Initializing;
            break;

        case State_Initializing:
            if (_initSubsystems()) _state = State_Running;
            break;

        case State_Running:
            _sensors.run();
            _ventilation.run();
            _serialMonitor.run();
            if (_dre.isEvent(Event_SystemHalted)) _state = State_Halted;
            break;

        case State_Halted:
            // poweroff or reboot
            break;
    }

    return _state != previous;
}


bool ReespiratorFsm::_initSubsystems()
{
    if (_settings.getState() <= SettingsManagerFsm::State_Loading)
    {
        _settings.run();
        return false;
    }

    if (_sensors.getState() == SensorsBuffer::State_None)
    {
        _sensors.run();
        return false;
    }

    if (_serialMonitor.getState() == Hmi01Fsm::State_None)
    {
        _serialMonitor.run();
        return false;
    }

    if (_ventilation.getState() == VentilationFsm::State_None)
    {
        _ventilation.run();
        return false;
    }

    return true;
}


}; // namespace reespirator
