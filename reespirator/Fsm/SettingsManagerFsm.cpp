/** SettingsManagerFsm.cpp
 *
 * FSM to manage machine settings.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "SettingsManagerFsm.hpp"

namespace reespirator
{

SettingsManagerFsm::SettingsManagerFsm(DRE& dre) :
    _dre(dre)
{
    _state = State_None;
    _lock = LockSource_None;
    _lockTimeoutSeconds = 0;

    // factory defaults
    _setFactoryDefaults();

    // DRE
    _dre.currentMachineParams = &_machineParams;
    _dre.currentAlarmLimits = &_alarmLimits;
}


bool SettingsManagerFsm::run()
{
    State _previous = _state;

    switch (_state)
    {
        case State_None:
            _state = State_Loading;
            break;

        case State_Loading:
            _loadSettings();
            _state = State_Unlocked;
            break;

        case State_Unlocked:
            _checkSources();
            break;

        case State_Locked:
            _checkLockTimeout();
            _checkSources();
            break;
    }

    return _previous == _state;
};


void SettingsManagerFsm::_setFactoryDefaults()
{
    // machine params
    _machineParams.setVentMode(PARAM_FACTORY_VENT_MODE);
    _machineParams.setVentControl(PARAM_FACTORY_VENT_CONTROL);
    _machineParams.setVolume(PARAM_FACTORY_VOLUME);
    _machineParams.setRpm(PARAM_FACTORY_RPM);
    _machineParams.setPeakPressure(PARAM_FACTORY_PEAK);
    _machineParams.setPeepPressure(PARAM_FACTORY_PEEP);
    _machineParams.setTriggerFlow(PARAM_FACTORY_FLOW_TRIGGER);
    _machineParams.setRamp(PARAM_FACTORY_RAMP);
    _machineParams.setIeRatioDivisor(PARAM_FACTORY_IE_RATIO_DIV);
    _machineParams.setRecruitTimer(0);
    _machineParams.setMutedAlarm(0);

    // alarm limits
    _alarmLimits.setVolumeLow(ALARM_FACTORY_VOLUME_LOW);
    _alarmLimits.setVolumeHigh(ALARM_FACTORY_VOLUME_HIGH);
    _alarmLimits.setRpmLow(ALARM_FACTORY_RPM_LOW);
    _alarmLimits.setRpmHigh(ALARM_FACTORY_RPM_HIGH);
    _alarmLimits.setPeakPressureLow(ALARM_FACTORY_PEAK_LOW);
    _alarmLimits.setPeakPressureHigh(ALARM_FACTORY_PEAK_HIGH);
    _alarmLimits.setPeepPressureLow(ALARM_FACTORY_PEEP_LOW);
    _alarmLimits.setPeepPressureHigh(ALARM_FACTORY_PEEP_HIGH);
    _alarmLimits.setBatteryLow(ALARM_FACTORY_BATTERY_LOW);
}


void SettingsManagerFsm::_checkSources()
{
    // consume alarm limits from serial monitor
    if (_dre.getSmAlarmLimitsRequest() != nullptr)
    {
        if (_tryLock(LockSource_ExternalHmi))
        {
            // check and update
            const AlarmLimits* al = _dre.getSmAlarmLimitsRequest();
            u8 error_code = _checkAlarmLimits(al);
            if (error_code == 0) _alarmLimits = *al;
            _dre.setSmAlarmLimitsResult(error_code);

            // lock for a while
            _lockTimeoutSeconds = getLoopSeconds() + LOCK_SOURCE_TIMEOUT_SECONDS;
        }
        else
        {
            // locked by another source
            _dre.setSmAlarmLimitsResult(AlarmLimits::ErrorCode_NotSet);
        }

        // clear request
        _dre.setSmAlarmLimitsRequest(nullptr);
    }

    // ToDo: consume alarm limits from local HMI

    // consume machine params from serial monitor
    if (_dre.getSmMachineParamsRequest() != nullptr)
    {
        if (_tryLock(LockSource_ExternalHmi))
        {
            // check and update
            const MachineParams* mp = _dre.getSmMachineParamsRequest();
            u8 error_code = _checkMachineParams(mp);
            if (error_code == MachineParams::ErrorCode_NoError) _machineParams = *mp;
            _dre.setSmMachineParamsResult(error_code);

            // lock for a while
            _lockTimeoutSeconds = getLoopSeconds() + LOCK_SOURCE_TIMEOUT_SECONDS;
        }
        else
        {
            // locked by another source
            _dre.setSmMachineParamsResult(MachineParams::ErrorCode_NotSet);
        }

        // clear request
        _dre.setSmMachineParamsRequest(nullptr);
    }
}


void SettingsManagerFsm::_checkLockTimeout()
{
    if (getLoopSeconds() >= _lockTimeoutSeconds)
    {
        // unlock
        _state = State_Unlocked;

        // save to persistent memory
        _saveSettings();
    }
}


bool SettingsManagerFsm::_tryLock(LockSource src)
{
    if (_lock == LockSource_None)
    {
        _lock = src;
        _state = State_Locked;
    }

    return (_lock == src);
}


u8 SettingsManagerFsm::_checkMachineParams(const MachineParams* mp) const
{
    if (mp->getVentMode() >= MachineParams::VentMode_NUM) return MachineParams::ErrorCode_VentMode;
    if (mp->getVentControl() >= MachineParams::VentControl_NUM) return MachineParams::ErrorCode_VentCtrl;
    if (mp->getVolume() < PARAM_VOLUME_MIN) return MachineParams::ErrorCode_Volume;
    if (mp->getVolume() > PARAM_VOLUME_MAX) return MachineParams::ErrorCode_Volume;
    if (mp->getRpm() < PARAM_RPM_MIN) return MachineParams::ErrorCode_Rpm;
    if (mp->getRpm() > PARAM_RPM_MAX) return MachineParams::ErrorCode_Rpm;
    if (mp->getPeakPressure() < PARAM_PEAK_MIN) return MachineParams::ErrorCode_Peak;
    if (mp->getPeakPressure() > PARAM_PEAK_MAX) return MachineParams::ErrorCode_Peak;
    if (mp->getPeepPressure() < PARAM_PEEP_MIN) return MachineParams::ErrorCode_Peep;
    if (mp->getPeepPressure() > PARAM_PEEP_MAX) return MachineParams::ErrorCode_Peep;
    if (mp->getTriggerFlow() < PARAM_FLOW_TRIGGER_MIN) return MachineParams::ErrorCode_TrigFlow;
    if (mp->getTriggerFlow() > PARAM_FLOW_TRIGGER_MAX) return MachineParams::ErrorCode_TrigFlow;
    if (mp->getRamp() < PARAM_RAMP_MIN) return MachineParams::ErrorCode_Ramp;
    if (mp->getRamp() > PARAM_RAMP_MAX) return MachineParams::ErrorCode_Ramp;
    if (mp->getIeRatioDivisor() < PARAM_IE_RATIO_DIV_MIN) return MachineParams::ErrorCode_IeDivisor;
    if (mp->getIeRatioDivisor() > PARAM_IE_RATIO_DIV_MAX) return MachineParams::ErrorCode_IeDivisor;
    if (mp->getRecruitTimer() > PARAM_RECRUIT_TIMER_MAX) return MachineParams::ErrorCode_RecrTimer;
    if (mp->getMutedAlarm() > PARAM_MUTED_ALARM_MAX) return MachineParams::ErrorCode_RecrTimer;

    return MachineParams::ErrorCode_NoError;
}


u8 SettingsManagerFsm::_checkAlarmLimits(const AlarmLimits* al) const
{
    // check limits
    if (al->getVolumeLow() < ALARM_VOLUME_MIN) return AlarmLimits::ErrorCode_VolumeLow;
    if (al->getVolumeHigh() > ALARM_VOLUME_MAX) return AlarmLimits::ErrorCode_VolumeHigh;
    if (al->getRpmLow() < ALARM_RPM_MIN) return AlarmLimits::ErrorCode_RpmLow;
    if (al->getRpmHigh() > ALARM_RPM_MAX) return AlarmLimits::ErrorCode_RpmHigh;
    if (al->getPeakPressureLow() < ALARM_PEAK_MIN) return AlarmLimits::ErrorCode_PeakLow;
    if (al->getPeakPressureHigh() > ALARM_PEAK_MAX) return AlarmLimits::ErrorCode_PeakHigh;
    if (al->getPeepPressureLow() < ALARM_PEEP_MIN) return AlarmLimits::ErrorCode_PeepLow;
    if (al->getPeepPressureHigh() > ALARM_PEEP_MAX) return AlarmLimits::ErrorCode_PeepHigh;

    return AlarmLimits::ErrorCode_NoError;
}



void SettingsManagerFsm::_loadSettings()
{
    // ToDo: use persistent memory
}


void SettingsManagerFsm::_saveSettings()
{
    // ToDo: use persistent memory
}



}; // namespace reespirator