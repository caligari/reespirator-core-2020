/** VentilationFsm.cpp
 *
 * Ventilation by PC (preassure control).
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */


#include "VentilationFsm.hpp"

namespace reespirator
{


VentilationFsm::VentilationFsm(DRE& dre) :
    _dre(dre)
{
    _state = State_None;

    _dre.lastCycle = &_lastCycle;
    _dre.currentVentilation = &_setPoint;
}


bool VentilationFsm::run()
{
    State previous = _state;

    // reset events
    _resetEvent(Event_InspirationStart);
    _resetEvent(Event_HalfCycle);
    _resetEvent(Event_ExpirationEnd);

    switch (_state)
    {
    case State_None:
        _state = State_EmptyCycle;
        break;

    case State_EmptyCycle:
        if (getVentilationMode() == MachineParams::VentMode_Running)
        {
            _startCycle();
            _state = State_Inspiration;
            _indicateEvent(Event_InspirationStart);
        }
        break;

    case State_Inspiration:
        if (!_doInspiration()) 
        {
            _state = State_Expiration;
            _indicateEvent(Event_HalfCycle);
        }
        break;

    case State_Expiration:
        if (!_doExpiration())
        {
            _closeCycle();
            _indicateEvent(Event_ExpirationEnd);
            _state = State_EmptyCycle;
        }
        break;
    }

    return previous != _state;
}


void VentilationFsm::_startCycle()
{
    _startTenths = _getCurrentTenths();
    u16 rpm10 = _getMachineParams()->getRpm();
    u16 iediv10 = _getMachineParams()->getIeRatioDivisor();

    // calculate inspiration-expiration durations
    u32 cycleMillis = 600000 / rpm10;
    _inspirationTenths= (cycleMillis * 10 / (10 + iediv10)) / 100;
    _expirationTenths = (cycleMillis / 100) - _inspirationTenths;
    _cycleTenths = _inspirationTenths + _expirationTenths;

    // precalc setpoints for cycle
    _control.setCycle(_inspirationTenths, _expirationTenths, _getMachineParams());
}


void VentilationFsm::_closeCycle()
{
    _lastCycle.setStart(_startTenths);
    _lastCycle.setDuration(_cycleTenths);
}


bool VentilationFsm::_doInspiration()
{
    // check time is out
    u16 lapTenths = _getCurrentTenths() - _startTenths;
    if (lapTenths > _inspirationTenths) return false;

    _setPoint.setPressure(_control.updateInspiration(lapTenths));

    return true;
}


bool VentilationFsm::_doExpiration()
{
    // check time is out
    u16 lapTenths = _getCurrentTenths() - _startTenths;
    if (lapTenths > _cycleTenths) return false;

    _setPoint.setPressure(_control.updateExpiration(lapTenths));

    return true;
}


}; // namespace reespirator
