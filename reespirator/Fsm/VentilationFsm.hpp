/** VentilationFsm.hpp
 *
 * Ventilation manager.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _VENTILATION_FSM_HPP
#define _VENTILATION_FSM_HPP

#include <reespirator/Core/DRE.hpp>
#include <reespirator/Core/Types/CycleData.hpp>
#include <reespirator/Ventilation/PressureControl.hpp>

namespace reespirator
{

class VentilationFsm
{
    public:

        VentilationFsm(DRE& dre);

        enum State
        {
            State_None,
            State_EmptyCycle,
            State_Inspiration,
            State_Expiration,
        };

        State getState() { return _state; }

        bool run();

    protected:

        // DRE input interface
        u32 _getCurrentTenths() { return _dre.runtime->getLoopTenths(); }
        const MachineParams* _getMachineParams() { return _dre.currentMachineParams; }
        u8 getVentilationMode() { return _dre.currentMachineParams->getVentMode(); }

        void _indicateEvent(u8 event) { _dre.setEvent(event, true); }
        void _resetEvent(u8 event) { _dre.setEvent(event, false); }

    private:

        DRE& _dre;

        State _state;

        // last cycle data
        CycleData _lastCycle;

        // current ventilation levels
        VentilationLevels _setPoint;

        // Control
        PressureControl _control;

        // current cycle
        u32 _startTenths;
        u16 _cycleTenths;
        u16 _inspirationTenths;
        u16 _expirationTenths;

        void _startCycle();
        void _closeCycle();

        bool _doInspiration();
        bool _doExpiration();
};

}; // namespace reespirato, lapTenthsr

#endif //_VENTILATION_FSM_HPP