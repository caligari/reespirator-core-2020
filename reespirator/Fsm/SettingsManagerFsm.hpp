/** SettingsManagerFsm.hpp
 *
 * Class to manage machine settigns.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _SETTINGS_MANAGER_FSM_HPP
#define _SETTINGS_MANAGER_FSM_HPP

#include <reespirator/Core/DRE.hpp>
#include <reespirator/Core/Types/AlarmLimits.hpp>

#define LOCK_SOURCE_TIMEOUT_SECONDS 10

namespace reespirator
{

/**
 * @brief Settings manager.
 *
 * This manager check when settings source is requesting changes and lock them to this source
 * or deny the request because a source gained the lock previously.
 *
 * The lock is kept for a while time to allow the source (probably controlled by an user) to
 * tune the settings.
 *
 * It also load and save the settings to the persistant storage.
 *
 */
class SettingsManagerFsm
{
    public:

        SettingsManagerFsm(DRE& dre);

        enum State
        {
            State_None,
            State_Loading,
            State_Unlocked,
            State_Locked,
        };

        State getState()
        {
            return _state;
        }

        bool run();

    protected:

        // DRE interface
        u32 getLoopSeconds() { return _dre.runtime->getLoopSeconds(); }

    private:

        DRE& _dre;

        State _state;

        MachineParams _machineParams;
        AlarmLimits _alarmLimits;

        /**
         * @brief Check if values for a new MachineParams settings are right.
         *
         * @param mp The new MachineParams.
         * @return ErrorCode indicating the error (see MachineParams::ErrorCode_*).
         */
        u8 _checkMachineParams(const MachineParams* mp) const;

        /**
         * @brief Check if values for a new AlarmLimits settings are right.
         *
         * @param al The new AlarmLimits.
         * @return ErrorCode indicating the error (see AlarmLimits::ErrorCode_*).
         */
        u8 _checkAlarmLimits(const AlarmLimits* al) const;

        /**
         * @brief Set the default factory settings
         */
        void _setFactoryDefaults();

        /**
         * @brief Check if any source is requesting a settings change.
         */
        void _checkSources();

        /**
         * @brief Check if settings lock can be released from unused source.
         */
        void _checkLockTimeout();

        /**
         * @brief Load settings from the persistent storage.
         */
        void _loadSettings();

        /**
         * @brief save settings to the persistent storage.
         */
        void _saveSettings();

        enum LockSource
        {
            LockSource_None,
            LockSource_LocalHmi,
            LockSource_ExternalHmi,
        };

        LockSource _lock;
        u32 _lockTimeoutSeconds;

        /**
         * @brief Try to adquire the lock to gain the control.
         *
         * @param src Source requiring the control.
         * @return true Control gained and locked.
         * @return false Control not gained.
         */
        bool _tryLock(LockSource src);

};

}; // namespace reespirator

#endif // _SETTINGS_MANAGER_FSM_HPP