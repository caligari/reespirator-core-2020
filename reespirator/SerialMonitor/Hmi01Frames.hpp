/** Hmi01Frames.hpp
 *
 * HMI v1 frame types.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _HMI_01_FRAMES_H
#define _HMI_01_FRAMES_H

#include <reespirator/Core/Types/Integers.hpp>

namespace reespirator {

enum FrameType : u8
{
    FrameType_AckError       = 0x00,
    FrameType_Request        = 0x01,
    FrameType_MachineInfo    = 0x02,
    FrameType_InstantMeasure = 0x03,
    FrameType_CycleData      = 0x04,
    FrameType_MachineParams  = 0x05,
    FrameType_AlarmParams    = 0x06,
    FrameType_AlarmStatus    = 0x07,
    FrameType_COUNT,
};


enum AckErrorCode : u8
{
    AckErrorCode_ACK          = 0x00,
    AckErrorCode_NACK         = 0x01,
    AckErrorCode_CrcError     = 0x02,
    AckErrorCode_BadApi       = 0x03,
    AckErrorCode_InvalidFrame = 0x04,
    AckErrorCode_Stalled      = 0x05,
};


struct FrameHeader
{
    u8 FixedValue;
    u8 ProtocolVersion;
    u8 FrameType;
    u8 FrameNumber;
} __attribute__((__packed__));


struct PayloadAckError
{
    u8 FrameReference;
    u8 AckErrorCode;
} __attribute__((__packed__));


struct PayloadRequest
{
    u8 RequestedFrameType;
} __attribute__((__packed__));


struct PayloadMachineInfo
{
    u8 MachineUuid[16];
    u16 FirmwareVersion;
    u16 UptimeMinutes15;
} __attribute__((__packed__));


struct PayloadInstantMeasure
{
    s16 Pressure;
    s16 Flow;
    u16 Volume;
} __attribute__((__packed__));


struct PayloadCycleData
{
    u16 Volume;
    u16 RpmLast30s;
    u16 PeakPressure;
    u16 PeepPressure;
    u16 PlateauPressure;
    u16 DrivingPressure;
    u8 Triggered;
    u32 StartTime;
    u16 Duration;
} __attribute__((__packed__));


struct PayloadMachineParams
{
    u8 VentControl;
    u8 VentMode;
    u16 Volume;
    u16 Rpm;
    u16 PeakPressure;
    u16 PeepPressure;
    u16 TriggerFlow;
    u8 Ramp;
    u8 IeRatioDivisor;
    u8 RecruitTimer;
    u8 MutedAlarmSeconds;
} __attribute__((__packed__));


struct PayloadAlarmLimits
{
    u16 VolumeLow;
    u16 VolumeHigh;
    u16 RpmLow;
    u16 RpmHigh;
    u16 PeakPressureLow;
    u16 PeakPressureHigh;
    u16 PeepPressureLow;
    u16 PeepPressureHigh;
    u8 BatteryLow;
} __attribute__((__packed__));


struct PayloadAlarmStatus
{
    u8 AlarmFlags[3];
} __attribute__((__packed__));


union FramePayload {
    PayloadAckError AckError;             // type 0
    PayloadRequest MachineRequest;        // type 1
    PayloadMachineInfo MachineInfo;       // type 2
    PayloadInstantMeasure InstantMeasure; // type 3
    PayloadCycleData CycleData;           // type 4
    PayloadMachineParams MachineParams;   // type 5
    PayloadAlarmLimits AlarmParams;       // type 6
    PayloadAlarmStatus AlarmStatus;       // type 7
};


#define FRAME_HEADER_SIZE (sizeof(FrameHeader))
#define FRAME_CHECKSUM_SIZE (sizeof(u16))
#define FRAME_NON_PAYLOAD_SIZE (FRAME_HEADER_SIZE + FRAME_CHECKSUM_SIZE)
#define FRAME_MAX_SIZE (sizeof(FramePayload) + FRAME_NON_PAYLOAD_SIZE)

static const u8 FRAME_SIZE[] =
{
    sizeof(PayloadAckError)       + FRAME_NON_PAYLOAD_SIZE, // type 0
    sizeof(PayloadRequest)        + FRAME_NON_PAYLOAD_SIZE, // type 1
    sizeof(PayloadMachineInfo)    + FRAME_NON_PAYLOAD_SIZE, // type 2
    sizeof(PayloadInstantMeasure) + FRAME_NON_PAYLOAD_SIZE, // type 3
    sizeof(PayloadCycleData)      + FRAME_NON_PAYLOAD_SIZE, // type 4
    sizeof(PayloadMachineParams)  + FRAME_NON_PAYLOAD_SIZE, // type 5
    sizeof(PayloadAlarmLimits)    + FRAME_NON_PAYLOAD_SIZE, // type 6
    sizeof(PayloadAlarmStatus)    + FRAME_NON_PAYLOAD_SIZE, // type 7
};

}; // namespace reespirator

#endif // _HMI_01_FRAMES_H