/** Hmi01Fsm.hpp
 *
 * Hmi01Fsm class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _HMI_01_FSM_HPP
#define _HMI_01_FSM_HPP

// this API version
#define API_VERSION 0x01

// milliseconds to wait for bus silence after resync event
#define RESYNC_TIMEOUT_MS 100

// seconds to wait for alarm ACK or resend alarms
#define ALARM_ACK_SECONDS 1

#include <reespirator/Core/DRE.hpp>
#include "Hmi01Frames.hpp"

namespace reespirator
{

class Hmi01Fsm
{
    public:

        /**
         * @brief Construct a new HMI FSM.
         *
         * @param dre Data Runtime Environment
         */
        Hmi01Fsm(DRE& dre);

        /**
         * @brief Non-blocking execution of FSM.
         *
         * @return true FSM state was changed.
         * @return false FSM state was not changed.
         */
        bool run();

        //////////////////////////////////////////////////////////////////////
        // states
        //////////////////////////////////////////////////////////////////////

        enum State : uint8_t
        {
            /**
             * @brief Not initialized.
             */
            State_None,

            /**
             * @brief Waiting for a 0x55 byte (start of frame).
             */
            State_WaitFor0x55,

            /**
             * @brief  Waiting for completing the header.
             *
             */
            State_WaitForHeader,

            /**
             * @brief Waiting for completing the payload and checksum.
             *
             */
            State_WaitForPayload,

            /**
             * @brief Processing a valid frame received.
             */
            State_ProcessFrame,

            /**
             * @brief Waiting for a silence to resynchronize comunication.
             */
            State_Resync,
        };

        /**
         * @brief Get the State of FSM.
         *
         * @return State
         */
        State getState()
        {
            return _state;
        }

    protected:

        // DRE interface
        CircularBuffer& getRxBuffer() { return _dre.getSmRxBuffer(); }
        CircularBuffer& getTxBuffer() { return _dre.getSmTxBuffer(); }
        const CycleData* getLastCycleData() { return _dre.lastCycle; }
        const MachineParams* getCurrentMachineParams() { return _dre.currentMachineParams; }
        const AlarmLimits* getCurrentAlarmLimits() { return _dre.currentAlarmLimits; }
        const InstantMeasures* getSensorMeasures() { return _dre.sensorMeasures; }
        const VentilationLevels* getCurrentVentilation() { return _dre.currentVentilation; }
        u32 getLoopMillis32() { return _dre.runtime->getLoopMillis32(); }
        const u8* getMachineUuid() { return _dre.runtime->getMachineUuid(); }
        u16 getFirmwareVersion() { return _dre.runtime->getFirmwareVersion(); }
        u32 getUptime15mCounter() { return _dre.runtime->getUptime15mCounter(); }
        void setEvent(u8 event, bool val) { _dre.setEvent(event, val); }

    private:

        DRE& _dre;
        State _state;

        u8 _rx[FRAME_MAX_SIZE];
        u8 _rxIndex;
        u8 _rxFrameType;
        u8 _rxFrameSize;

        u8 _tx[FRAME_MAX_SIZE];
        u8 _txCounter;

        MachineParams _mpRequest;
        bool _isPendingMpRequest;
        u8 _pendingMpRequestFrame;

        AlarmLimits _alRequest;
        bool _isPendingAlRequest;
        u8 _pendingAlRequestFrame;

        u32 _resyncTimeout;

        bool _eventAlarmsChanged;
        bool _eventCycleNew;
        bool _eventMeasuresNew;

        u32 _alarmAckTimeout;
        u8 _alarmAckFrame;

        bool _validateHeader();
        bool _validatePayload();

        bool _sendFrame(FrameType type);
        bool _sendAckError(AckErrorCode code, u8 frame_reference);
        bool _sendAckError(AckErrorCode code) { return _sendAckError(code, _rx[3]); }

        void _processFrame();
        void _processAckError(const PayloadAckError* payload);
        void _processRequest(const PayloadRequest* payload);
        void _processMachineParams(const PayloadMachineParams* payload);
        void _processAlarmParams(const PayloadAlarmLimits* payload);

        void _sendEvents();
        bool _sendPendingAcks();
        bool _sendFrameMachineInfo();
        bool _sendFrameInstantMeasure();
        bool _sendFrameCycleData();
        bool _sendFrameMachineParams();
        bool _sendFrameAlarmLimits();
        bool _sendFrameAlarmStatus();

        bool _isAlarmAckTimeout()
        {
            return (_alarmAckTimeout > 0 && getLoopMillis32() >= _alarmAckTimeout);
        }

        u8 _getTxFrameNumber()
        {
            return _tx[0x03];
        }
};

}; // namespace reespirator

#endif // _HMI_01_FSM_HPP