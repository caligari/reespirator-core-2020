/** Hmi01Fsm.cpp
 *
 * Hmi01Fsm class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "Hmi01Fsm.hpp"
#include <reespirator/Core/Helpers/Crc16.hpp>
#include <string.h>

#define CRC16_INITIAL 0xffff
#define START_OF_FRAME 0x55

namespace reespirator
{

Hmi01Fsm::Hmi01Fsm(DRE& dre) :
    _dre(dre)
{
    _state = State_None;
    _txCounter = 0;
    _resyncTimeout = 0;
    _isPendingMpRequest = false;
    _isPendingAlRequest = false;
    _alarmAckTimeout = 0;

    _eventAlarmsChanged = false;
    _eventCycleNew = false;
    _eventMeasuresNew = false;
}


bool Hmi01Fsm::run()
{
    State previous = getState();

    setEvent(Event_SerialTxOverflow, false);

    size_t received = 1;
    while (received > 0)
    {
        received = 0;
        switch (_state)
        {
            case State_None:
                _state = State_WaitFor0x55;
                break;

            case State_WaitFor0x55:
                if (getRxBuffer().skipUntil(START_OF_FRAME))
                {
                    _rx[0] = START_OF_FRAME;
                    received = _rxIndex = 1;
                    _state = State_WaitForHeader;
                }
                break;

            case State_WaitForHeader:
                received = getRxBuffer().popBytes(_rx + _rxIndex, FRAME_HEADER_SIZE - _rxIndex);
                _rxIndex += received;
                if (_rxIndex == FRAME_HEADER_SIZE)
                {
                    _state = (_validateHeader() ? State_WaitForPayload : State_Resync);
                }
                break;

            case State_WaitForPayload:
                received += getRxBuffer().popBytes(_rx + _rxIndex, _rxFrameSize - _rxIndex);
                _rxIndex += received;
                if (_rxIndex == _rxFrameSize)
                {
                    _state = (_validatePayload() ? State_ProcessFrame : State_Resync);
                }
                break;

            case State_ProcessFrame:
                _processFrame();
                _state = State_WaitFor0x55;
                received = 0;
                break;

            case State_Resync:
                if (_resyncTimeout == 0)
                {
                    // flush and wait resync timeout
                    _dre.getSmRxBuffer().reset();
                    _resyncTimeout = getLoopMillis32() + RESYNC_TIMEOUT_MS;
                }
                else
                {
                    if (_dre.getSmRxBuffer().getBytesCount() > 0)
                    {
                        // extend resync timeout while not silence
                        _resyncTimeout = getLoopMillis32() + RESYNC_TIMEOUT_MS;
                        _dre.getSmRxBuffer().reset();
                    }
                    else if (getLoopMillis32() >= _resyncTimeout)
                    {
                        // resync timeout is done
                        _resyncTimeout = 0;
                        _state = State_WaitFor0x55;
                    }
                }
                break;

        }; // switch

    }; // while

    // events in this side to notify the monitor
    if (_dre.isEvent(Event_AlarmsChanged)) _eventAlarmsChanged = true;
    if (_dre.isEvent(Event_ExpirationEnd)) _eventCycleNew = true;
    if (_dre.isEvent(Event_MeasuresNew)) _eventMeasuresNew = true;
    _sendEvents();

    return getState() == previous;
}


bool Hmi01Fsm::_validateHeader()
{
    const FrameHeader* header = (FrameHeader*)_rx;

    // protocol version must match
    if (header->ProtocolVersion != API_VERSION)
    {
        _sendAckError(AckErrorCode_BadApi);
        return false;
    }

    // validate frame type
    if (header->FrameType >= FrameType_COUNT)
    {
        _sendAckError(AckErrorCode_InvalidFrame);
        return false;
    }

    // header OK
    _rxFrameType = header->FrameType;
    _rxFrameSize = FRAME_SIZE[header->FrameType];

    return true;
}


bool Hmi01Fsm::_validatePayload()
{
    // checksum
    const u8 crc_pos = _rxFrameSize - 2;
    const u16 frame_crc = _rx[crc_pos] | (_rx[crc_pos + 1] << 8);
    const u16 calc_crc = Crc16::calc(CRC16_INITIAL, _rx, _rxFrameSize - 2);
    if (frame_crc != calc_crc)
    {
        _sendAckError(AckErrorCode_CrcError);
        return false;
    }

    // payload OK
    return true;
}


bool Hmi01Fsm::_sendFrame(FrameType type)
{
    // header
    FrameHeader* header = (FrameHeader*) _tx;
    header->FixedValue = START_OF_FRAME;
    header->ProtocolVersion = API_VERSION;
    header->FrameType = type;
    header->FrameNumber = _txCounter++;

    // checksum
    size_t size = FRAME_SIZE[type] - 2;
    u16 crc = Crc16::calc(CRC16_INITIAL, _tx, size);
    _tx[size++] = crc & 0x00ff;
    _tx[size++] = crc >> 8;

    // send to UART port
    bool ok = (getTxBuffer().pushBytes(_tx, size) == size);

    // TX overflow
    if (!ok) setEvent(Event_SerialTxOverflow, true);

    return ok;
}


bool Hmi01Fsm::_sendAckError(AckErrorCode code, u8 frame_reference)
{
    PayloadAckError* payload = (PayloadAckError*) (_tx + FRAME_HEADER_SIZE);

    payload->FrameReference = frame_reference;
    payload->AckErrorCode = code;

    return _sendFrame(FrameType_AckError);
}


void Hmi01Fsm::_processFrame()
{
    const void* payload_ptr = _rx + FRAME_HEADER_SIZE;

    switch (_rxFrameType)
    {
        case FrameType_AckError:
            _processAckError((const PayloadAckError*)payload_ptr);
            break;

        case FrameType_Request:
            _processRequest((const PayloadRequest*)payload_ptr);
            break;

        case FrameType_MachineParams:
            _processMachineParams((const PayloadMachineParams*)payload_ptr);
            break;

        case FrameType_AlarmParams:
            _processAlarmParams((const PayloadAlarmLimits*)payload_ptr);
            break;

        default:
            _sendAckError(AckErrorCode_InvalidFrame);
            break;
    };
}


void Hmi01Fsm::_processAckError(const PayloadAckError* payload)
{
    switch (payload->AckErrorCode)
    {
        case AckErrorCode_ACK:
            if (_alarmAckTimeout > 0 && payload->FrameReference == _alarmAckFrame)
            {
                // deactivate resending alarms
                _alarmAckTimeout = 0;
            }
            break;

        default:
            // ignore others
            break;
    }
}


void Hmi01Fsm::_processRequest(const PayloadRequest* payload)
{
    switch (payload->RequestedFrameType)
    {
        case FrameType_MachineInfo:
            _sendFrameMachineInfo();
            break;

        case FrameType_CycleData:
            _sendFrameCycleData();
            break;

        case FrameType_MachineParams:
            _sendFrameMachineParams();
            break;

        case FrameType_AlarmParams:
            _sendFrameAlarmLimits();
            break;

        case FrameType_AlarmStatus:
            _sendFrameAlarmStatus();
            break;

        default:
            _sendAckError(AckErrorCode_InvalidFrame);
            break;
    };
}


void Hmi01Fsm::_processMachineParams(const PayloadMachineParams* payload)
{
    // create request
    _mpRequest.setVentMode(payload->VentMode);
    _mpRequest.setVentControl(payload->VentControl);
    _mpRequest.setVolume(payload->Volume);
    _mpRequest.setRpm(payload->Rpm);
    _mpRequest.setPeakPressure(payload->PeakPressure);
    _mpRequest.setPeepPressure(payload->PeepPressure);
    _mpRequest.setTriggerFlow(payload->TriggerFlow);
    _mpRequest.setRamp(payload->Ramp);
    _mpRequest.setIeRatioDivisor(payload->IeRatioDivisor);
    _mpRequest.setRecruitTimer(payload->RecruitTimer);
    _mpRequest.setMutedAlarm(payload->MutedAlarmSeconds);
    _dre.setSmMachineParamsRequest(&_mpRequest);

    // remember request is pending to send response when done
    _isPendingMpRequest = true;
    _pendingMpRequestFrame = _getTxFrameNumber();
}


void Hmi01Fsm::_processAlarmParams(const PayloadAlarmLimits* payload)
{
    // create request
    _alRequest.setVolumeLow(payload->VolumeLow);
    _alRequest.setVolumeHigh(payload->VolumeHigh);
    _alRequest.setRpmLow(payload->RpmLow);
    _alRequest.setRpmHigh(payload->RpmHigh);
    _alRequest.setPeakPressureLow(payload->PeakPressureLow);
    _alRequest.setPeakPressureHigh(payload->PeakPressureHigh);
    _alRequest.setPeepPressureLow(payload->PeepPressureLow);
    _alRequest.setPeepPressureHigh(payload->PeepPressureHigh);
    _alRequest.setBatteryLow(payload->BatteryLow);
    _dre.setSmAlarmLimitsRequest(&_alRequest);

    // remember request is pending to send response when done
    _isPendingAlRequest = true;
    _pendingAlRequestFrame = _getTxFrameNumber();
}


void Hmi01Fsm::_sendEvents()
{
    if (!_sendPendingAcks())
    {
        if (_eventAlarmsChanged || _isAlarmAckTimeout())
        {
            // send alarms when changed or resend if not ACK back
            _sendFrameAlarmStatus();
            _eventAlarmsChanged = false;

            // remember this frame to resend if not ack received back in ALARM_ACK_SECONDS
            _alarmAckFrame = _getTxFrameNumber();
            _alarmAckTimeout = getLoopMillis32() + (ALARM_ACK_SECONDS * 1000);
        }
        else if (_eventCycleNew)
        {
            // send new cycle data
            _sendFrameCycleData();
            _eventCycleNew = false;
        }
        else if (_eventMeasuresNew)
        {
            // send new measures
            _sendFrameInstantMeasure();
            _eventMeasuresNew = false;
        }
    }
}


bool Hmi01Fsm::_sendPendingAcks()
{
    bool sent = false;

    // ACK when request processed by settings manager
    if (_isPendingMpRequest && _dre.getSmMachineParamsRequest() == nullptr)
    {
        AckErrorCode error_code = (AckErrorCode) _dre.getSmMachineParamsResult();
        _sendAckError(error_code, _pendingMpRequestFrame);
        _isPendingMpRequest = false;
        sent = true;
    }

    // ACK when request processed by settings manager
    if (_isPendingAlRequest && _dre.getSmAlarmLimitsRequest() == nullptr)
    {
        AckErrorCode error_code = (AckErrorCode) _dre.getSmAlarmLimitsResult();
        _sendAckError(error_code, _pendingAlRequestFrame);
        _isPendingAlRequest = false;
        sent = true;
    }

    return sent;
}


bool Hmi01Fsm::_sendFrameMachineInfo()
{
    PayloadMachineInfo* payload = (PayloadMachineInfo*) (_tx + FRAME_HEADER_SIZE);

    memcpy(payload->MachineUuid, getMachineUuid(), sizeof(payload->MachineUuid));
    payload->FirmwareVersion = getFirmwareVersion();
    payload->UptimeMinutes15 = getUptime15mCounter();

    return _sendFrame(FrameType_MachineInfo);
}


bool Hmi01Fsm::_sendFrameInstantMeasure()
{
    PayloadInstantMeasure* payload = (PayloadInstantMeasure*) (_tx + FRAME_HEADER_SIZE);

    const InstantMeasures* sensors = getSensorMeasures();
    const VentilationLevels* ventilation = getCurrentVentilation();

    payload->Pressure = sensors->getPressureMeasure();
    payload->Flow = sensors->getFlowMeasure();
    payload->Volume = ventilation->getVolume();

    return _sendFrame(FrameType_InstantMeasure);
}


bool Hmi01Fsm::_sendFrameCycleData()
{
    PayloadCycleData* payload = (PayloadCycleData*) (_tx + FRAME_HEADER_SIZE);
    const CycleData* last_cycle = getLastCycleData();

    payload->Volume = last_cycle->getVolume();
    payload->RpmLast30s = last_cycle->getRpmLast30s();
    payload->PeakPressure = last_cycle->getPeakPressure();
    payload->PeepPressure = last_cycle->getPeepPressure();
    payload->PlateauPressure = last_cycle->getPlateauPressure();
    payload->DrivingPressure = last_cycle->getDrivingPressure();
    payload->Triggered = last_cycle->wasTriggered();
    payload->StartTime = last_cycle->getStart();
    payload->Duration = last_cycle->getDuration();

    return _sendFrame(FrameType_CycleData);
}


bool Hmi01Fsm::_sendFrameMachineParams()
{
    PayloadMachineParams* payload = (PayloadMachineParams*) (_tx + FRAME_HEADER_SIZE);
    const MachineParams* params = getCurrentMachineParams();

    payload->VentControl = params->getVentControl();
    payload->VentMode = params->getVentMode();
    payload->Volume = params->getVolume();
    payload->Rpm = params->getRpm();
    payload->PeakPressure = params->getPeakPressure();
    payload->PeepPressure = params->getPeepPressure();
    payload->TriggerFlow = params->getTriggerFlow();
    payload->Ramp = params->getRamp();
    payload->IeRatioDivisor = params->getIeRatioDivisor();
    payload->RecruitTimer = params->getRecruitTimer();
    payload->MutedAlarmSeconds = params->getMutedAlarm();

    return _sendFrame(FrameType_MachineParams);
}


bool Hmi01Fsm::_sendFrameAlarmLimits()
{
    PayloadAlarmLimits* payload = (PayloadAlarmLimits*) (_tx + FRAME_HEADER_SIZE);
    const AlarmLimits* limits = getCurrentAlarmLimits();

    payload->VolumeLow = limits->getVolumeLow();
    payload->VolumeHigh = limits->getVolumeHigh();
    payload->RpmLow = limits->getRpmLow();
    payload->RpmHigh = limits->getRpmHigh();
    payload->PeakPressureLow = limits->getPeakPressureLow();
    payload->PeakPressureHigh = limits->getPeakPressureHigh();
    payload->PeepPressureLow = limits->getPeepPressureLow();
    payload->PeepPressureHigh = limits->getPeepPressureHigh();
    payload->BatteryLow = limits->getBatteryLow();

    return _sendFrame(FrameType_AlarmParams);
}


bool Hmi01Fsm::_sendFrameAlarmStatus()
{
    PayloadAlarmStatus* payload = (PayloadAlarmStatus*) (_tx + FRAME_HEADER_SIZE);

    static const u8 ALARM_BITS[] =
    {
        // byte 0
        Alarm_VolumeLow, Alarm_VolumeHigh, Alarm_RpmLow, Alarm_RpmHigh,
        Alarm_PeakLow, Alarm_PeakHigh, Alarm_PeepLow, Alarm_PeepHigh,

        // byte 1
        Alarm_BatteryLow, Alarm_Paused, Alarm_PreassureFail, Alarm_VolumeFail,
        Alarm_MechanicalFail, Alarm_ValveFail, Alarm_Homing, Alarm_Panic,

        // byte 2
        Alarm_InputFlowLow, Alarm_InputFlowHigh,
    };

    unsigned int alarm_index = 0;
    u8 byte = 0;
    while (byte < sizeof(payload->AlarmFlags))
    {
        u8 flags = 0x00;
        int bit = 0x01;
        while (bit <= 0xff && alarm_index < sizeof(ALARM_BITS))
        {
            if (_dre.isAlarm(ALARM_BITS[alarm_index++])) flags |= bit;
            bit <<= 1;
        }
        payload->AlarmFlags[byte++] = flags;
    }

    return _sendFrame(FrameType_AlarmStatus);
}


}; // namespace reespirator
