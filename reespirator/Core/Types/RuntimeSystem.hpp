/** RuntimeSystem.hpp
 *
 * Container for runtime system variables.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _RUNTIME_SYSTEM_HPP
#define _RUNTIME_SYSTEM_HPP

namespace reespirator
{

class RuntimeSystem
{
    public:

        RuntimeSystem() { setLoopMillis64(0); }

        /**
         * @brief Version number (high byte is major, low byte is minor).
         * @see https://gitlab.com/reespirator/firmware/arduino-2020/-/releases
         */
        u16 getFirmwareVersion() const { return _firmwareVersion; }
        void setFirmwareVersion(u16 val) { _firmwareVersion = val; }

        /**
         * @brief 16-byte structure containing the UUID for this machine.
         * @see https://en.wikipedia.org/wiki/Universally_unique_identifier
         */
        const u8* getMachineUuid() const { return _machineUuid; }
        void setMachineUuid(const u8* bytes16) { memcpy (_machineUuid, bytes16, 16); }

        /**
         * @brief Uptime counter (15 minutes step).
         */
        u16 getUptime15mCounter() const { return _uptime15mCounter; }
        void setUptime15mCounter(u16 val) { _uptime15mCounter = val; }

        /**
         * @brief Get the milliseconds (32 bits size) of start for the current main loop.
         */
        u32 getLoopMillis32() const { return (u32)_loopMillis; }

        /**
         * @brief Get the tenths of second from start for the current main loop.
         */
        u32 getLoopTenths() const { return _loopTenths; }

        /**
         * @brief Get the seconds of start for the current main loop.
         */
        u32 getLoopSeconds() const { return _loopSeconds; }

        /**
         * @brief Get the milliseconds (64 bits size) of start for the current main loop.
         */
        u64 getLoopMillis64() const { return _loopMillis; }
        void setLoopMillis64(u64 val)
        {
            _loopMillis = val;
            _loopTenths = val / 100;
            _loopSeconds = _loopTenths / 10;
        }

    private:

        u8 _machineUuid[16];
        u16 _firmwareVersion;
        u16 _uptime15mCounter;
        u64 _loopMillis;
        u32 _loopTenths;
        u32 _loopSeconds;

};

}; // namespace reespirator

#endif // _RUNTIME_SYSTEM_HPP