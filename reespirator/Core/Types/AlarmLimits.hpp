/** AlarmLimits.hpp
 *
 * Container structure to Alarm limits.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _ALARM_LIMITS_HPP
#define _ALARM_LIMITS_HPP

#define ALARM_VOLUME_MIN 100
#define ALARM_VOLUME_MAX 900
#define ALARM_FACTORY_VOLUME_LOW 100
#define ALARM_FACTORY_VOLUME_HIGH 900

#define ALARM_RPM_MIN 3
#define ALARM_RPM_MAX 30
#define ALARM_FACTORY_RPM_LOW 3
#define ALARM_FACTORY_RPM_HIGH 30

#define ALARM_PEAK_MIN 9
#define ALARM_PEAK_MAX 35
#define ALARM_FACTORY_PEAK_LOW 9
#define ALARM_FACTORY_PEAK_HIGH 35

#define ALARM_PEEP_MIN 4
#define ALARM_PEEP_MAX 25
#define ALARM_FACTORY_PEEP_LOW 4
#define ALARM_FACTORY_PEEP_HIGH 25

#define ALARM_FACTORY_BATTERY_LOW 20

#include "Integers.hpp"

namespace reespirator
{

class AlarmLimits
{
    public:

        /**
         * @brief Error codes (0x60-0x6f) when setting AlarmLimits.
         */
        enum
        {
            ErrorCode_NoError    = 0x00,
            ErrorCode_NotSet     = 0x60,
            ErrorCode_VolumeLow  = 0x61,
            ErrorCode_VolumeHigh = 0x62,
            ErrorCode_RpmLow     = 0x63,
            ErrorCode_RpmHigh    = 0x64,
            ErrorCode_PeakLow    = 0x65,
            ErrorCode_PeakHigh   = 0x66,
            ErrorCode_PeepLow    = 0x67,
            ErrorCode_PeepHigh   = 0x68,
        };

        /**
         * @brief Volume Low alarm setting. If the Volume measure is lower, an alarm is raised.
         * Units are ml (millilitres).
         */
        u16 getVolumeLow() const { return _volumeLow; }
        void setVolumeLow(u16 val) { _volumeLow = val; }

        /**
         * @brief Volume High alarm setting. If the Volume measure is higher, an alarm is raised.
         * Units are ml (millilitres).
         */
        u16 getVolumeHigh() const { return _volumeHigh; }
        void setVolumeHigh(u16 val) { _volumeHigh = val; }

        /**
         * @brief RPM Low alarm setting. If the RPM measure is lower, an alarm is raised.
         * Units are respirations (breaths) per minute. Unsigned integer coded with 1 decimal position.
         */
        u16 getRpmLow() const { return _rpmLow; }
        void setRpmLow(u16 val) { _rpmLow = val; }

        /**
         * @brief RPM High alarm setting. If the RPM measure is higher, an alarm is raised.
         * Units are respirations (breaths) per minute. Unsigned integer coded with 1 decimal position.
         */
        u16 getRpmHigh() const { return _rpmHigh; }
        void setRpmHigh(u16 val) { _rpmHigh = val; }

        /**
         * @brief Peak Pressure Low alarm setting. If the Peak Pressure measure is lower, an alarm is
         * raised. Units are cmH2O. Unsigned integer coded with 1 decimal position.
         */
        u16 getPeakPressureLow() const { return _peakPressureLow; }
        void setPeakPressureLow(u16 val) { _peakPressureLow = val; }

        /**
         * @brief Peak Pressure High alarm setting. If the Peak Pressure measure is higher, an alarm is
         * raised. Units are cmH2O. Unsigned integer coded with 1 decimal position.
         */
        u16 getPeakPressureHigh() const { return _peakPressureHigh; }
        void setPeakPressureHigh(u16 val) { _peakPressureHigh = val; }

        /**
         * @brief Peep Pressure Low alarm setting. If the Peep Pressure measure is lower, an alarm is
         * raised. Units are cmH2O. Unsigned integer coded with 1 decimal position.
         */
        u16 getPeepPressureLow() const { return _peepPressureLow; }
        void setPeepPressureLow(u16 val) { _peepPressureLow = val; }

        /**
         * @brief Peep Pressure High alarm setting. If the Peep Pressure measure is higher, an alarm is
         * raised. Units are cmH2O. Unsigned integer coded with 1 decimal position.
         */
        u16 getPeepPressureHigh() const { return _peepPressureHigh; }
        void setPeepPressureHigh(u16 val) { _peepPressureHigh = val; }

        /**
         * @brief Battery Low alarm setting. If the Battery measure is lower, an alarm is raised. Unit
         * is charge % (percent).
         */
        u8 getBatteryLow() const { return _batteryLow; }
        void setBatteryLow(u8 val) { _batteryLow = val; }

        /**
         * @brief Compare value with other AlarmLimits object
         *
         * @param other The other instance to compare.
         * @return true Both are equal.
         * @return false They are not equal.
         */
        bool operator==(const AlarmLimits& other) const
        {
            return (other._volumeLow == _volumeLow)
                && (other._volumeHigh == _volumeHigh)
                && (other._rpmLow == _rpmLow)
                && (other._rpmHigh == _rpmHigh)
                && (other._peakPressureLow == _peakPressureLow)
                && (other._peakPressureHigh == _peakPressureHigh)
                && (other._peepPressureLow == _peepPressureLow)
                && (other._peepPressureHigh == _peepPressureHigh)
                && (other._batteryLow == _batteryLow);
        }

    private:

        u16 _volumeLow;
        u16 _volumeHigh;
        u16 _rpmLow;
        u16 _rpmHigh;
        u16 _peakPressureLow;
        u16 _peakPressureHigh;
        u16 _peepPressureLow;
        u16 _peepPressureHigh;
        u8 _batteryLow;
};

}; // namespace reespirator

#endif // _ALARM_PARAMS_HPP
