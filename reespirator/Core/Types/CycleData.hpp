/** CycleData.hpp
 *
 * Container structure to cycle data.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _CYCLE_DATA_HPP
#define _CYCLE_DATA_HPP

namespace reespirator
{

class CycleData
{
    public:

        /**
         * @brief PIP (Peak Inspiratory Pressure) measured in the last completed
         * respiration cycle. Units are cmH2O. Unsigned integer coded with 2 decimal
         * positions.
         *
         * @return u16 The peak pressure.
         */
        u16 getPeakPressure() const { return _peakPressure; }
        void setPeakPressure(u16 val ) { _peakPressure = val; }

        /**
         * @brief PEEP (Positive End-Expiratory Pressure) measured in the last completed
         * respiration cycle. Units are cmH2O. Unsigned integer coded with 2 decimal
         * positions.
         *
         * @return u16  The peep pressure.
         */
        u16 getPeepPressure() const { return _peepPressure; }
        void setPeepPressure(u16 val) { _peepPressure = val; }

        /**
         * @brief Plateau Pressure measured in the last completed respiration cycle.
         * Units are cmH2O. Unsigned integer coded with 2 decimal positions.
         *
         * @return u16  The plateau pressure.
         */
        u16 getPlateauPressure() const { return _plateauPressure; }
        void setPlateauPressure(u16 val) { _plateauPressure = val; }

        /**
         * @brief Driving Pressure measured in the last completed respiration cycle.
         * Units are cmH2O. Unsigned integer coded with 2 decimal positions.
         *
         * @return u16 The plateau pressure.
         */
        u16 getDrivingPressure() const { return _drivingPressure; }
        void setDrivingPressure(u16 val) { _drivingPressure = val; }

        /**
         * @brief Tidal Volume measured in the last completed respiration cycle. Units
         * are millilitres.
         *
         * @return u16  The volume.
         */
        u16 getVolume() const { return _volume; }
        void setVolume(u16 val) { _volume = val; }

        /**
         * @brief Average RPM calculated over the last 30 seconds. Units are respirations
         * (breaths) per minute. Unsigned integer coded with 1 decimal position.
         *
         * @return u16 The RPM over last 30s.
         */
        u16 getRpmLast30s() const { return _rpmLast30s; }
        void setRpmLast30s(u16 val) { _rpmLast30s = val; }

        /**
         * @brief Indication when current respiration cycle was triggered.
         *
         * @return true Triggered.
         * @return false  Not triggered.
         */
        bool wasTriggered() const { return _triggered; }
        void setTriggered(bool val) { _triggered = val; }

        /**
         * @brief Time in tenths of second since machine was started.
         *
         * @return u32 The start time.
         */
        u32 getStart() const { return _start; }
        void setStart(u32 val) { _start = val; }

        /**
         * @brief Cycle duration in tenths of seconds since cycle start time.
         *
         * @return u16 The cycle time.
         */
        u16 getDuration() const { return _duration; }
        void setDuration(u16 val) { _duration = val; }

    private:

        u16 _peakPressure;
        u16 _peepPressure;
        u16 _plateauPressure;
        u16 _drivingPressure;
        u16 _volume;
        u16 _rpmLast30s;
        bool _triggered;
        u32 _start;
        u16 _duration;
};

}; // namespace reespirator

#endif // _CYCLE_DATA_HPP