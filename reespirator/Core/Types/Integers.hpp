/** Integers.hpp
 *
 * Integer literals.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _INTEGERS_HPP
#define _INTEGERS_HPP

#include <stdint.h>

namespace reespirator {

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t   s8;
typedef int16_t  s16;
typedef int32_t  s32;
typedef int64_t  s64;

#define INT_BITS (sizeof(int) * 8)

}; // namespace reespirator

#endif // _INTEGERS_HPP
