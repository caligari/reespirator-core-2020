/** VentilationLevels.hpp
 *
 * Container for ventilation levels.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _VENTILATION_LEVELS_HPP
#define _VENTILATION_LEVELS_HPP

namespace reespirator
{

class VentilationLevels
{
    public:

        /**
         * @brief Accumulated volume for current cycle. Unit is ml with 0 decimal positions.
         *
         * @return u16
         */
        u16 getVolume() const { return _volume; }
        void setVolume(u16 val) { _volume = val; }

        /**
         * @brief Pressure set point for current cycle. Unit is cmH2O with 2 decimal positions.
         *
         * @return u16
         */
        u16 getPressure() const { return _pressure; }
        void setPressure(u16 val) { _pressure = val; }

    private:

        u16 _volume;
        u16 _pressure;
};

}; // namespace reespirator

#endif // _VENTILATION_LEVELS_HPP