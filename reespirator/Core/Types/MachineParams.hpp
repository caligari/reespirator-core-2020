/** MachineParams.hpp
 *
 * Structure of machine parameters.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _MACHINE_PARAMS_HPP
#define _MACHINE_PARAMS_HPP

// millilitres
#define PARAM_VOLUME_MIN 100
#define PARAM_VOLUME_MAX 900
#define PARAM_FACTORY_VOLUME 380

// respirations per minute x 10
#define PARAM_RPM_MIN (3 * 10)
#define PARAM_RPM_MAX (30 * 10)
#define PARAM_FACTORY_RPM (12 * 10)

// cmH20 x 100
#define PARAM_PEAK_MIN (9 * 100)
#define PARAM_PEAK_MAX (35 * 100)
#define PARAM_FACTORY_PEAK (28 * 100)

// cmH2O x 100
#define PARAM_PEEP_MIN (4 * 100)
#define PARAM_PEEP_MAX (25 * 100)
#define PARAM_FACTORY_PEEP (6 * 100)

// lmp
#define PARAM_FLOW_TRIGGER_MIN 3
#define PARAM_FLOW_TRIGGER_MAX 19
#define PARAM_FACTORY_FLOW_TRIGGER 3

// inspiration proportion * 255
#define PARAM_RAMP_MIN 1
#define PARAM_RAMP_MAX 254
#define PARAM_FACTORY_RAMP 127

// divisor x 10
#define PARAM_IE_RATIO_DIV_MIN (1 * 10)
#define PARAM_IE_RATIO_DIV_MAX (4 * 10)
#define PARAM_FACTORY_IE_RATIO_DIV (2 * 10)

// seconds
#define PARAM_RECRUIT_TIMER_MAX 40
#define PARAM_MUTED_ALARM_MAX 40

// ventilation control
#define PARAM_FACTORY_VENT_MODE MachineParams::VentMode_Stopped
#define PARAM_FACTORY_VENT_CONTROL MachineParams::VentControl_PC

#include "Integers.hpp"

namespace reespirator
{

class MachineParams
{
    public:

        /**
         * @brief Ventilation mode param values.
         */
        enum
        {
            VentMode_Stopped = 0,
            VentMode_Running = 1,
            VentMode_Paused  = 2,
            VentMode_NUM
        };


        /**
         * @brief Ventilation control param values.
         */
        enum
        {
            VentControl_PC   = 0,
            VentControl_VC   = 1,
            VentControl_VCRP = 2,
            VentControl_NUM
        };

        /**
         * @brief Error codes (0x50-0x5f) when setting MachineParams.
         */
        enum
        {
            ErrorCode_NoError      = 0x00,
            ErrorCode_NotSet     = 0x50,
            ErrorCode_VentMode   = 0x51,
            ErrorCode_VentCtrl   = 0x52,
            ErrorCode_Volume     = 0x53,
            ErrorCode_Rpm        = 0x54,
            ErrorCode_Peak       = 0x55,
            ErrorCode_Peep       = 0x56,
            ErrorCode_TrigFlow   = 0x57,
            ErrorCode_Ramp       = 0x58,
            ErrorCode_IeDivisor  = 0x59,
            ErrorCode_RecrTimer  = 0x5a,
            ErrorCode_MutedAlarm = 0x5b,
        };


        /**
         * @brief Get the Ventilation Mode Param. See VentMode_* values.
         */
        u8 getVentMode() const { return _ventMode; }
        void setVentMode(u8 val) { _ventMode = val; }

        /**
         * @brief Get the Ventilation Control . See VentControl_* values.
         */
        u8 getVentControl() const { return _ventControl; }
        void setVentControl(u8 val) { _ventControl = val; }

        /**
         * @brief Tidal Volume (ml).
         */
        u16 getVolume() const { return _volume; }
        void setVolume(u16 val) { _volume = val; }

        /**
         * @brief Respirations per minute. Integer coded with 1 decimal position.
         */
        u16 getRpm() const { return _rpm; }
        void setRpm(u16 val) { _rpm = val; }

        /**
         * @brief Peak pressure (cmH2O). Integer coded with 2 decimal positions.
         */
        u16 getPeakPressure() const { return _peakPressure; }
        void setPeakPressure(u16 val) { _peakPressure = val; }

        /**
         * @brief Peep pressure (cmH2O). Integer coded with 2 decimal positions.
         */
        u16 getPeepPressure() const { return _peepPressure; }
        void setPeepPressure(u16 val) { _peepPressure = val; }

        /**
         * @brief Threshold flow (lpm) to detect triggered cycles.
         */
        u16 getTriggerFlow() const { return _triggerFlow; }
        void setTriggerFlow(u16 val) { _triggerFlow = val; }

        /**
         * @brief Proportion of inspiration time (ranged from 0 to 255) dedicated to
         * increase the pressure from peep to peak. Unsigned integer.
         */
        u8 getRamp() const { return _ramp; }
        void setRamp(u8 val) { _ramp = val; }

        /**
         * @brief Inspiratory/Expiratory ratio. Integer coded with 1 decimal position.
         */
        u8 getIeRatioDivisor() const { return _ieRatioDivisor; }
        void setIeRatioDivisor(u8 val) { _ieRatioDivisor = val; }

        /**
         * @brief Recruit maneuver timer in seconds.
         */
        u8 getRecruitTimer() const { return _recruitTimer; }
        void setRecruitTimer(u8 val) { _recruitTimer = val; }

        /**
         * @brief Muted alarm timer (seconds).
         */
        u8 getMutedAlarm() const { return _mutedAlarm; }
        void setMutedAlarm(u8 val) { _mutedAlarm = val; }

        /**
         * @brief Compare value with other MachineParams object
         *
         * @param other The other instance to compare.
         * @return true Both are equal.
         * @return false They are not equal.
         */
        bool operator==(const MachineParams& other) const
        {
            return (other._ventMode == _ventMode)
                && (other._ventControl == _ventControl)
                && (other._volume == _volume)
                && (other._rpm == _rpm)
                && (other._peakPressure == _peakPressure)
                && (other._peepPressure == _peepPressure)
                && (other._triggerFlow == _triggerFlow)
                && (other._ramp == _ramp)
                && (other._ieRatioDivisor == _ieRatioDivisor)
                && (other._recruitTimer == _recruitTimer)
                && (other._mutedAlarm == _mutedAlarm);
        }


    private:

        u8 _ventMode;
        u8 _ventControl;
        u16 _volume;
        u16 _rpm;
        u16 _peakPressure;
        u16 _peepPressure;
        u16 _triggerFlow;
        u8 _ramp;
        u8 _ieRatioDivisor;
        u8 _recruitTimer;
        u8 _mutedAlarm;

};

}; // namespace reespirator

#endif // _MACHINE_PARAMS_HPP