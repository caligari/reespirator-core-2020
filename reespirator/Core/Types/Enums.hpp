/** Enums.hpp
 *
 * Enumerations used in reespirator namespace.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _ENUMS_HPP
#define _ENUMS_HPP

namespace reespirator {


/**
 * @brief Alarm_* flags
 */
enum
{
    Alarm_VolumeLow = 0,
    Alarm_VolumeHigh = 1,
    Alarm_RpmLow = 2,
    Alarm_RpmHigh = 3,
    Alarm_PeakLow = 4,
    Alarm_PeakHigh = 5,
    Alarm_PeepLow = 6,
    Alarm_PeepHigh = 7,
    Alarm_BatteryLow = 8,
    Alarm_Paused = 9,
    Alarm_PreassureFail = 10,
    Alarm_VolumeFail = 11,
    Alarm_MechanicalFail = 12,
    Alarm_ValveFail = 13,
    Alarm_Homing = 14,
    Alarm_Panic = 15,
    Alarm_InputFlowLow = 16,
    Alarm_InputFlowHigh = 17,

    Alarm_COUNT // the last one to count the alarms
};



/**
 * @brief Event_* flags
 */
enum
{
    Event_SystemHalted,
    Event_AlarmsChanged,
    Event_InspirationStart,
    Event_HalfCycle,
    Event_ExpirationEnd,
    Event_MeasuresNew,
    Event_SerialTxOverflow,

    Event_COUNT // the last one to count the events
};


}; // namespace reespirator

#endif // _ENUMS_HPP