/** CircularBuffer.hpp
 *
 * CircularBuffer class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _CIRCULAR_BUFFER_HPP
#define _CIRCULAR_BUFFER_HPP

#include <reespirator/Core/Types/Integers.hpp>

// default type to manage buffer addresses.
#ifndef BUFFER_ADDRESS_TYPE
#define BUFFER_ADDRESS_TYPE u8
#endif

namespace reespirator {

/**
 * @brief Class to manage a memory buffer as a circular buffer.
 */
class CircularBuffer
{
    public:

        /**
         * @brief Type to address the whole byte positions on the buffer.
         */
        typedef BUFFER_ADDRESS_TYPE size_t;


        /**
         * @brief Construct a new circular buffer over an array of memory bytes.
         *
         * @param buffer Pointer to the array of memory bytes.
         * @param buffer_size Size of the buffer in bytes.
         */
        CircularBuffer(u8* buffer, size_t buffer_size) :
            _bufferPtr(buffer),
            _bufferSize(buffer_size)
        {
            reset();
        }


        /**
         * @brief Get the current occupied size in the buffer.
         *
         * @return size_t Number of bytes in the buffer.
         */
        size_t getBytesCount() const
        {
            return _count;
        }


        /**
         * @brief Try to pop a byte from the buffer.
         *
         * @param destination Byte reference where to copy the byte.
         * @return true Byte was popped.
         * @return false Byte was NOT popped.
         */
        bool popByte(u8& destination)
        {
            if (_count > 0)
            {
                destination = _bufferPtr[_start++];
                _start %= _bufferSize;
                _count--;
                return true;
            }

            return false;
        }


        /**
         * @brief Try to pop some bytes from buffer to destination.
         *
         * @param destination Array where to put the popped bytes.
         * @param size Largest count of bytes to pop.
         * @return size_t The real count of popped bytes
         */
        size_t popBytes(u8* destination, size_t size)
        {
            if (size > getBytesCount()) size = getBytesCount();

            for (size_t b = 0; b < size; b++)
            {
                destination[b] = _bufferPtr[_start++];
                _start %= _bufferSize;
            }

            _count -= size;

            return size;
        }


        /**
         * @brief Get the current free size in the buffer.
         *
         * @return size_t Number of free byte positions in the buffer.
         */
        size_t getEmptySpace() const
        {
            return _bufferSize - _count;
        }


        /**
         * @brief Try to push a byte into the buffer.
         *
         * @param byte The byte to push.
         * @return true Byte was pushed.
         * @return false Byte was NOT pushed (buffer is full).
         */
        bool pushByte(u8 byte)
        {
            if (_count < _bufferSize)
            {
                size_t head = (_start + _count) % _bufferSize;
                _bufferPtr[head] = byte;
                _count++;
                return true;
            }

            return false;
        }


        /**
         * @brief Try to push an array of bytes into the buffer.
         *
         * @param source  Pointer to the source array of bytes.
         * @param size The size of source array.
         * @return size_t The real count of pushed bytes
         */
        size_t pushBytes(const u8* source, size_t size)
        {
            if (size > getEmptySpace()) size = getEmptySpace();

            size_t head = (_start + _count) % _bufferSize;
            for (size_t b = 0; b < size; b++)
            {
                _bufferPtr[head++] = source[b];
                head %= _bufferSize;
            }

            _count += size;

            return size;
        }


        /**
         * @brief Reset the buffer to empty status.
         */
        void reset()
        {
            _start = 0;
            _count = 0;
        }


        /**
         * @brief Consume buffered bytes until a byte is found.
         *
         * @param found_byte Byte to found.
         * @return true Byte was found.
         * @return false Byte was not found.
         */
        bool skipUntil(u8 found_byte)
        {
            while (_count > 0)
            {
                u8 current = _bufferPtr[_start++];
                _start %= _bufferSize;
                _count--;

                if (current == found_byte) return true;
            }

            return false;
        }


        /**
         * @brief Peek the buffer for the next byte (without pop).
         *
         * @param destination Byte reference where to copy the next byte.
         * @return true Byte exists.
         * @return false Buffer is empty.
         */
        bool peekByte(u8& byte)
        {
            if (_count > 0)
            {
                byte = _bufferPtr[_start];
                return true;
            }

            return false;
        }


        /**
         * @brief Try to peek some bytes from buffer to destination.
         *
         * @param destination Array where to put the peeked bytes.
         * @param size Largest count of bytes to peek.
         * @return size_t The real count of peeked bytes
         */
        size_t peekBytes(u8* destination, size_t size)
        {
            if (size > getBytesCount()) size = getBytesCount();
            size_t start = _start;

            for (size_t b = 0; b < size; b++)
            {
                destination[b] = _bufferPtr[start++];
                start %= _bufferSize;
            }

            return size;
        }


    private:

        u8* _bufferPtr;
        const size_t _bufferSize;

        size_t _start;
        size_t _count;
};

}; // namespace reespirator

#endif // CIRCULAR_BUFFER_HPP