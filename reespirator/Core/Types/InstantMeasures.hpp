/** InstantMeasures.hpp
 *
 * Measures from sensors.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _INSTANT_MEASURES_HPP
#define _INSTANT_MEASURES_HPP

namespace reespirator
{

class InstantMeasures
{
    public:

        /**
         * @brief Current pressure. Unit is cmH2O. Integer coded with 2 decimal positions.
         */
        s16 getPressureMeasure() const { return _pressureMeasure; }
        void setPressureMeasure(s16 val) { _pressureMeasure = val; }

        /**
         * @brief Current flow. Unit is lpm (litres-per-minute). Integer coded with 2 decimal positions.
         */
        s16 getFlowMeasure() const { return _flowMeasure; }
        void setFlowMeasure(s16 val) { _flowMeasure = val; }

    private:

        s16 _pressureMeasure;
        s16 _flowMeasure;

};

}; // namespace reespirator

#endif // _INSTANT_MEASURES_HPP