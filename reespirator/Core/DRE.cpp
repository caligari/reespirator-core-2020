/** DRE.cpp
 *
 * Data runtime environment.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "DRE.hpp"

namespace reespirator 
{

DRE::DRE() :
    _smRxBuffer(_smRxArray, SM_RX_BUFFER_SIZE),
    _smTxBuffer(_smTxArray, SM_TX_BUFFER_SIZE)
{
}

}; // namespace reespirator 