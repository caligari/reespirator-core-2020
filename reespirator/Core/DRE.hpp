/** DRE.hpp
 *
 * Data runtime environment.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _DRE_HPP
#define _DRE_HPP

#include <string.h>
#include "Types/Integers.hpp"
#include "Types/Enums.hpp"
#include "Types/CircularBuffer.hpp"
#include "Types/MachineParams.hpp"
#include "Types/AlarmLimits.hpp"
#include "Types/CycleData.hpp"
#include "Types/InstantMeasures.hpp"
#include "Types/VentilationLevels.hpp"
#include "Types/RuntimeSystem.hpp"

#ifndef SM_RX_BUFFER_SIZE
#define SM_RX_BUFFER_SIZE 100
#endif

#ifndef SM_TX_BUFFER_SIZE
#define SM_TX_BUFFER_SIZE 100
#endif


namespace reespirator
{

class DRE
{
    public:

        /**
         * @brief Construct a new DRE object.
         */
        DRE();

        //////////////////////////////////////////////////////////////////////
        // SerialMonitor module
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Get the SerialMonitor RX buffer.
         *
         * Writter: SerialPort
         *
         * @return CircularBuffer& The SerialMonitor RX buffer.
         */
        CircularBuffer& getSmRxBuffer()
        {
            return _smRxBuffer;
        }

        /**
         * @brief Get the SerialMonitor TX buffer.
         *
         * Writter: SerialMonitor
         *
         * @return CircularBuffer& The SerialMonitor TX buffer.
         */
        CircularBuffer& getSmTxBuffer()
        {
            return _smTxBuffer;
        }

        /**
         * @brief Pointer to a request of MachineParams coming from serial monitor.
         *
         * @return MachineParams* The request or nullptr.
         */
        const MachineParams* getSmMachineParamsRequest() const
        {
            return _smMachineParamsRequest;
        }

        /**
         * @brief Set a request of MachineParams from serial monitor. SerialMonitor produces
         * by setting the pointer and SettingsManager consumes by setting it to nullptr.
         *
         * @param val Pointer to the MachineParams (set) or nullptr (clear).
         */
        void setSmMachineParamsRequest(const MachineParams* val)
        {
            _smMachineParamsRequest = val;
        }

        /**
         * @brief Get the result for a request of MachineParams.
         *
         * @return u8 Code for result.
         */
        u8 getSmMachineParamsResult()
        {
            return _smMachineParamsResult;
        }

        /**
         * @brief Set the result for a request of MachineParams.
         *
         * @param val  Code for result.
         */
        void setSmMachineParamsResult(u8 val)
        {
            _smMachineParamsResult = val;
        }

        /**
         * @brief Pointer to a request of AlarmLimits coming from serial monitor.
         *
         * @return AlarmLimits* The request or nullptr.
         */
        const AlarmLimits* getSmAlarmLimitsRequest() const
        {
            return _smAlarmLimitsRequest;
        }

        /**
         * @brief Set a request of AlarmLimits from serial monitor. SerialMonitor produces
         * by setting the pointer and SettingsManager consumes by setting it to nullptr.
         *
         * @param val Pointer to the AlarmLimits (set) or nullptr (clear).
         */
        void setSmAlarmLimitsRequest(const AlarmLimits* val)
        {
            _smAlarmLimitsRequest = val;
        }

        /**
         * @brief Get the result for a AlarmLimits request.
         *
         * @return u8 Code for result.
         */
        u8 getSmAlarmLimitsResult()
        {
            return _smAlarmLimitsResult;
        }

        /**
         * @brief Set the result for a AlarmLimits request.
         *
         * @param val Code for result.
         */
        void setSmAlarmLimitsResult(u8 val)
        {
            _smAlarmLimitsResult = val;
        }

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        bool isEvent(u8 event)
        {
            return (_eventFlags[event / INT_BITS] & (1 << (event % INT_BITS))) != 0;
        }


        void setEvent(u8 event, bool val)
        {
            if (val)
            {
                _eventFlags[event / INT_BITS] |= (1 << (event % INT_BITS));
            }
            else
            {
                _eventFlags[event / INT_BITS] &= ~(1 << (event % INT_BITS));
            }
        }


        //////////////////////////////////////////////////////////////////////
        ///// Alarms
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Test if an alarm flag is active.
         *
         * @param alarm Alarm to test (use Alarm_*).
         * @return true Alarm is active.
         * @return false  Alarm is not active.
         */
        bool isAlarm(u8 alarm)
        {
            return (_alarmFlags[alarm / INT_BITS] & (1 << (alarm % INT_BITS))) != 0;
        }


        /**
         * @brief Set or clear an alarm flag.
         *
         * @param alarm Alarm to set or clear. See Alarm_*
         * @param val True to set. False to clear.
         */
        void setAlarm(u8 alarm, bool val)
        {
            if (val)
            {
                _alarmFlags[alarm / INT_BITS] |= (1 << (alarm % INT_BITS));
            }
            else
            {
                _alarmFlags[alarm / INT_BITS] &= ~(1 << (alarm % INT_BITS));
            }
        }


        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Last cycle data.
         *
         * Writer: reespirator::VentilationManager
         */
        const CycleData* lastCycle;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Measures received from sensors.
         *
         * Writer: reespirator::SensorsController
         */
        const InstantMeasures* sensorMeasures;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Levels related to the current ventilation cycle.
         *
         * Writer: reespirator::VentilationManager
         */
        const VentilationLevels* currentVentilation;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Current machine params
         *
         * Writer: reespirator::SettingsManager
         */
        const MachineParams* currentMachineParams;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Current alarm limits.
         *
         * Writer: reespirator::SettingsManager
         */
        const AlarmLimits* currentAlarmLimits;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

        /**
         * @brief Runtime system variables.
         *
         * Writer: reespirator::VentilationMachine
         */
        const RuntimeSystem* runtime;

        //////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////

    private:

        //////////////////////////////////////////////////////////////////////
        // SM - SerialMonitor module
        //////////////////////////////////////////////////////////////////////

        u8 _smRxArray[SM_RX_BUFFER_SIZE];
        u8 _smTxArray[SM_TX_BUFFER_SIZE];

        CircularBuffer _smRxBuffer;
        CircularBuffer _smTxBuffer;

        const MachineParams* _smMachineParamsRequest;
        u8 _smMachineParamsResult;

        const AlarmLimits* _smAlarmLimitsRequest;
        u8 _smAlarmLimitsResult;

        //////////////////////////////////////////////////////////////////////
        ///// Alarms
        //////////////////////////////////////////////////////////////////////

        int _alarmFlags[((Alarm_COUNT - 1) / INT_BITS) + 1];

        //////////////////////////////////////////////////////////////////////
        ///// Events
        //////////////////////////////////////////////////////////////////////

        int _eventFlags[((Event_COUNT - 1) / INT_BITS) + 1];
};

}; // namespace reespirator

#endif //_DRE_HPP