/** SensorsController.hpp
 *
 * Sensors controller class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _SENSORS_BUFFER_HPP
#define _SENSORS_BUFFER_HPP

#include <reespirator/Core/DRE.hpp>
#include <LoopTicker.hpp>

namespace reespirator
{

/**
 * @brief This class simulates
 *
 */
class SensorsBuffer
{
    public:

        /**
         * @brief Construct a new Sensors Controller FSM.
         *
         * @param dre Data Runtime Environment
         */
        SensorsBuffer(DRE& dre);

        /**
         * @brief Non-blocking execution of FSM.
         *
         * @return true FSM state was changed.
         * @return false FSM state was not changed.
         */
        bool run();

        enum State
        {
            State_None,
            State_Initializing,
            State_Sensing,
        };

        /**
         * @brief Get the State of FSM.
         *
         * @return State
         */
        State getState() { return _state; }

    protected:

        // DRE interface
        u32 _getCurrentTenths() { return _dre.runtime->getLoopTenths(); }
        u16 _getPressureSetpoint() { return _dre.currentVentilation->getPressure(); }
        void _setEvent(u8 event, bool value) { _dre.setEvent(event, value); }

    private:

        DRE& _dre;
        State _state;
        InstantMeasures _measures;

        u32 _averagePressure;

        void _simulate();
};

}; // namespace reespirator

#endif // _SENSORS_BUFFER_HPP