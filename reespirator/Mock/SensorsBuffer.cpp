/** SensorsController.cpp
 *
 * Sensors controller class.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#include "SensorsBuffer.hpp"

namespace reespirator
{

SensorsBuffer::SensorsBuffer(DRE& dre) :
    _dre(dre)
{
    _state = State_None;
    _measures.setPressureMeasure(0);
    _measures.setFlowMeasure(0);
    _averagePressure = 0;

    dre.sensorMeasures = &_measures;
}


bool SensorsBuffer::run()
{
    State previous = getState();

    switch (_state)
    {
        case State_None:
            _state = State_Initializing;
            break;

        case State_Initializing:
            // ToDo: init
            _state = State_Sensing;
            break;

        case State_Sensing:
            _simulate();
            break;
    };

    return getState() == previous;
}


void SensorsBuffer::_simulate()
{
    _setEvent(Event_MeasuresNew, false);

    static u32 last_tenth = 0;
    u32 tenth = _getCurrentTenths();
    if (last_tenth != tenth)
    {
        // every 2 tenths
        if (tenth % 2 == 0)
        {
            // average over last 4 setpoints
            _averagePressure *= 3;
            _averagePressure += (_getPressureSetpoint() << 8);
            _averagePressure /= 4;

            _measures.setPressureMeasure(_averagePressure >> 8);

            _setEvent(Event_MeasuresNew, true);
        
        }

        last_tenth = tenth;
    }
}

}; // namespace reespirator
